# Cómo publicar una entrada nueva

Para publicar un post nuevo debes seguir los siguientes pasos

1. **Clona Sugusweb**

    `git clone https://gitlab.com/sugus_gitlab/sugusweb.git`

    Si tienes 2FA activado y permiso sobre el repo necesitarás tener SSH
    configurado

    `git@gitlab.com:sugus_gitlab/sugusweb.git`

2. **Crea una rama nueva para el post**

    La idea de usar una rama nueva que salga de master y para evitar conflictos
    trata que el nuevo post sea un solo commit, de esta forma el histórico de
    master se mantendrá más limpio y claro durante más tiempo.

    Luego al mergear esta nueva rama en master debe ser por fast-forward.

3. **Crea el fichero de tu nuevo post**

    Los posts están contenidos en `content/post/*.md`. Para crear un post nuevo
    recomendamos usar `hugo`. Bien puedes instalarlo o usar una imagen de
    Docker, en este caso recomendamos
    [klakegg/hugo](https://hub.docker.com/r/klakegg/hugo/)

    Sustituyendo yyyy-mm-dd por la fecha y filename por el nombre de fichero (ten
    en cuenta que el nombre del fichero no es el título del post).
    
    `hugo new content/post/yyyy-mm-dd-filename.md`

    También puedes copiar un post anterior y cambiar tanto el frontmatter
    (principio del post delimitado por `---`) como el nombre del fichero,
    conservando el formato de la fecha.

4. **Pruébalo en local antes de commitear nada**

    Recuerda que Hugo te permite montar un servidor local para probar el sitio
    web con el que estás trabajando.
    Teniendo la raíz de la web como directorio actual, y usando la imagen de
    Docker. `-D` sirve para mostrar aquellos posts marcados como `draft=true`.

    ```
    docker run --rm -it -v $(pwd):/src -p 1313:1313 klakegg/hugo:0.53 server -D
    ```

5. **Una vez listo, trae los cambios a master**

    Si tienes permiso en el repositorio por participar dentro de SUGUS no te
    supondrá mayor problema.

    1. Haz checkout de master: `git checkout master`

    2. Mergea la rama del post (debería hacer fast-forward): `git merge {rama_post}`

    En caso de ser una contribución externa y no tener permisos sobre el
    repositorio tan solo tienes que hacer el merge request de tu rama del post
    remota sobre la rama master de este repositorio.


Tras todo esto, en cuanto llegue el cambio a master comenzará un pipeline de
GitlabCI que contendrá los jobs encargados de generar la página web y
desplegarla. Puedes leer más sobre este proceso específicamente en
[docs/ci.md](docs/ci.md).
