---
title: Introducción a la linea de comandos con Bash
description: Aprende desde cero a moverte por el terminal como pez en el agua y conoce toda la potencia que oculta 
date: 2019-10-22
image: cartel-introterminal.png
categories:
    - Actividades
tags:
    - talleres
---

La interfaz de linea de comandos es uno de los elementos archiconocidos dentro del mundo de la informática, desde la proliferación de las primeras terminales por los años 70 hasta los infames hackers todopoderosos de las peliculas de Hollywood. ¿Pero es algo que podamos usar en nuestro dia a dia en la actualidad?

¡Claro que sí!

La linea de comandos es mucho más que una terrorifica cosa del pasado, es una herramienta polivalente y rápida que facilita muchas de nuestras tareas cotidianas y consigue hacer posible otras muchas que serian bastante dificiles de otra forma. Desde automatizar varias tareas con un solo comando hasta planificar toda la configuración de red o gestionar en remoto un servidor sin entorno gráfico, la interfaz de linea de comandos es una herramienta versatil y al alcance de todo el mundo.

Este taller introductorio tiene un enfoque bastante práctico. Se explicaran y probarán algunos de los comandos más usados en un sistema Linux, con los que se cubrirán algunos de los ambitos de uso más frecuentes de la terminal: navegación del sistema, manipulación de archivos, uso de pipelines y redirección, etc. Esta pensado para personas que sea claro y ameno a personas que nunca hayan usado una linea de comandos, asi que no te preocupes y animate :)

Se usará durante el taller el interpete de comandos Bash, utilizado por defecto en la amplia mayoria de distribuciones GNU/Linux. Recomendamos seguir el taller utilizando un pc propio con SO Linux o bien los ordenadores disponibles en el aula donde se realizará el taller. No obstante, aquellos que quieran seguir el taller desde un sistema Windows tienen la opción de preparar su equipo para utilizar Bash mediante el Windows Subsistem for Linux (WSL).

¡Os esperamos!

## Horarios y aula

- 11 de octubre de 2019 a las 17:30

- Aula B1.35

- Ponente: Víctor López Jiménez

## Material utilizado en el taller

https://gitlab.com/sugus_gitlab/charlas/introduccion-a-la-consola-de-comandos
