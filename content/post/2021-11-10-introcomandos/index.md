---
title: Introducción a la línea de comandos
description: 
date: "2021-11-11"
image: cartel-introcomandos.png
categories:
    - Actividades
tags:
    - talleres
---

## Materiales necesarios

En este taller se trabajará con el interprete de comandos de un sistema Linux. Podéis traer vuestro propio ordenador o bien usar los que hay disponibles en el aula.

## Horarios y aula

- Miércoles 17 de noviembre de 2021 a las 17:30

- Escuela Técnica Superior de Ingeniería Informática

- Aula: B1.35

## Ponente

El taller lo imparte José María Guisado Gómez, miembro de SUGUS.
